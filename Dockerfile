FROM ubuntu:22.04 AS final

WORKDIR /home/coder

# Set timezone, because docker is stupid with timezones
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## TMP install openjdk-21-jdk maven for Keycloak Training, not required otherwise
RUN apt-get update && apt-get install -y curl wget gnupg software-properties-common vim python3 python3-pip bash-completion nano sudo unzip openjdk-21-jdk maven && apt-get clean
RUN add-apt-repository ppa:git-core/ppa
RUN apt-get update && apt-get install -y git git-lfs && apt-get clean

# Install Terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install terraform

# Install Exoscale cli
# renovate: datasource=github-releases depName=exoscale/cli
ENV EXOSCALE_CLI_VERSION=1.79.1
RUN wget https://github.com/exoscale/cli/releases/download/v${EXOSCALE_CLI_VERSION}/exoscale-cli_${EXOSCALE_CLI_VERSION}_linux_amd64.deb && dpkg -i exoscale-cli_${EXOSCALE_CLI_VERSION}_linux_amd64.deb

# Install docker
RUN curl -fsSL https://get.docker.com | sh

# Install docker-compose plugin
RUN apt-get update && apt-get install -y docker-compose-plugin && apt-get clean

# Install code-server
RUN curl -fsSL https://code-server.dev/install.sh | sh
RUN code-server --install-extension ms-azuretools.vscode-docker
RUN code-server --install-extension mhutchie.git-graph
RUN code-server --install-extension vscjava.vscode-java-pack

# Dummy workdir
RUN mkdir /home/coder/workspace

# Autocompletion and aliases
RUN { echo "source /etc/bash_completion" ; echo 'alias docker-compose="docker compose"' ; echo 'alias dc="docker compose"' ;} >>  /root/.bashrc

# unminimize ubuntu for manpages
RUN yes | unminimize

# Gitconfig
RUN git config --global init.defaultBranch main
RUN git config --global core.editor "code-server --wait"

RUN mkdir /commandhistory \
    && touch /commandhistory/.bash_history \
    && echo "export PROMPT_COMMAND='history -a' && export HISTFILE=/commandhistory/.bash_history" >> /root/.bashrc

EXPOSE 8080
ENTRYPOINT code-server --bind-addr 0.0.0.0:8080 /home/coder/workspace

FROM final AS security
RUN apt-get update && apt-get -y full-upgrade && apt-get -y autoremove
